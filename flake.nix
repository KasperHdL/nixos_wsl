{
  inputs = 
  {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";
#    nixpkgs-unstable.url = "nixpkgs/nixos-unstable";

    nixos-wsl = 
    {
      url = "github:nix-community/NixOS-WSL";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixvim =
    {
      url = "github:nix-community/nixvim/nixos-23.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, nixos-wsl, nixvim, ... }@inputs: 
  {
    nixosConfigurations.nixos = nixpkgs.lib.nixosSystem 
    {
      system = "x86_64-linux";
      modules = 
      [
        nixos-wsl.nixosModules.wsl
        ./configuration.nix
        ./nixvim.nix
        nixvim.nixosModules.nixvim
      ];
    };
  };
}
