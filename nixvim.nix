{pkgs, ...}:
{
  programs.nixvim = {
    enable = true;
    viAlias = true;
    vimAlias = true;

    colorschemes.dracula.enable = true;
    
    plugins =
    {
      lightline.enable = true;

      zig.enable = true;

      lspkind.enable = true;
      lsp = {
        enable = true;
        servers = {
          nixd.enable = true;
          zls = 
          {
            enable = true;
            autostart = true;
          };
        };
      };
      coq-nvim = 
      {
        enable = true;
        autoStart = true;
      };

    };

    extraConfigVim =
    ''
      set cursorline
      set nobackup
      set noswapfile
      set relativenumber

      set ignorecase
      set smartcase

      set ai
      set si

      set tabstop=4
      set expandtab
      set signcolumn=yes:1

      let mapleader="\<space>"
    '';

    extraPlugins = with pkgs.vimPlugins; 
    [
      vim-nix
      vim-easymotion
      ctrlp
    ];

    keymaps = 
    [
      {
        mode = "i";
        key = "jk";
        options.silent = true;
        action = "<esc>";
      }
      {
        mode = "n";
        key = "<C>p";
        action = ":CtrlP<cr>";
      }
      {
        mode = "n";
        key = "<leader>s";
        action = "<Plug>(easymotion-overwin-f2)";
      }
    ];
  };
}
